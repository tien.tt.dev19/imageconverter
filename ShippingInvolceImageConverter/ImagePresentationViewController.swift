//
//  ImagePresentationViewController.swift
//  ShippingInvolceImageConverter
//
//  Created by Tiến Trần on 14/05/2023.
//

import UIKit

class ImagePresentationViewController: UIViewController {
    
    @IBOutlet weak var img_ImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func onPresentImage(images: [UIImage]) {
        DispatchQueue.main.async {
            self.img_ImageView.animationImages = images
            self.img_ImageView.animationDuration = 2.0 // The total duration of the animation in seconds
            self.img_ImageView.animationRepeatCount = 0 // 0 means repeat indefinitely
            self.img_ImageView.startAnimating()
        }
    }
}
