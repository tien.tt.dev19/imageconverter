//
//  ViewController.swift
//  ShippingInvolceImageConverter
//
//  Created by Tiến Trần on 14/05/2023.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tf_TextFiled: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tf_TextFiled.delegate = self
    }
    
    @IBAction func onConvertImage(_ sender: UIButton) {
        if let pdfLink = self.tf_TextFiled.text {
            if let images = ImageConverterManager.shared.imageConvert(pdfURL: pdfLink) {
                let controller = ImagePresentationViewController(nibName: "ImagePresentationViewController", bundle: nil)
                controller.onPresentImage(images: images)
                self.present(controller, animated: true)
            } else {
                self.onPresentAlert(with: "Convert ảnh thất bại")
            }
        } else {
            self.onPresentAlert(with: "Vui lòng nhập link pdf")
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
}

extension ViewController {
    private func onPresentAlert(with message: String) {
        let alertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)

//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
//            // Handle cancel action
//        }

        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            // Handle OK action
        }

//        alertController.addAction(cancelAction)
        alertController.addAction(okAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

