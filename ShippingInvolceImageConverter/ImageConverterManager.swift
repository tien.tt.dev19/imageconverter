//
//  ImageConverterManager.swift
//  ShippingInvolceImageConverter
//
//  Created by Tiến Trần on 14/05/2023.
//

import Foundation
import UIKit
import PDFKit

@objcMembers
class ImageConverterManager: NSObject {
    public static let shared = ImageConverterManager()
    
    public func imageConvert(pdfURL: String) -> [UIImage]? {
        guard let urlString = URL(string: pdfURL) else {
            return nil
        }
        
        if let pdf = PDFDocument(url: urlString) {
            var images = [UIImage]()
            for i in 0 ..< pdf.pageCount {
                guard let page = pdf.page(at: i) else {
                    continue
                }
                let pageSize = page.bounds(for: .cropBox).size
                
                let renderer = UIGraphicsImageRenderer(size: pageSize)
                let image = renderer.image { context in
                    UIColor.white.set()
                    context.fill(CGRect(origin: .zero, size: pageSize))
                    context.cgContext.translateBy(x: 0, y: pageSize.height)
                    context.cgContext.scaleBy(x: 1, y: -1)
                    context.cgContext.drawPDFPage(page.pageRef!)
                }
                images.append(image)
            }
            return images
        } else {
            return nil
        }
    }
}
